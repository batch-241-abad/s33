console.log("hello world")



/*
	SECTION js synchronous vs asynchronous

	asynchronous means that we can proceed to execute other statements,
	while time consuming code is running in the background

	fetch() method returns a promise that resolves to a response object

	promise - is an object that represents the eventual completion or failure of an asynchronouse function and its resulting value
*/
console.log(fetch("https://jsonplaceholder.typicode.com/posts/"))


// fetch() method returns a promise that resolves to a "response object"
fetch("https://jsonplaceholder.typicode.com/posts/")
// then() captures the ""
.then(response => console.log(response.status));



fetch("https://jsonplaceholder.typicode.com/posts/")
.then(response => response.json())
.then((json) => console.log(json));


// ASYNC AND AWAIT

async function fetchData(){
	let result = await fetch("https://jsonplaceholder.typicode.com/posts/")
	console.log(result);
	console.log(typeof result);
	console.log(result.body);

	let json = await result.json();
	console.log(json);
}
fetchData();

// creating a post


fetch("https://jsonplaceholder.typicode.com/posts/", {
	method: "POST",
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		"userId": 1,
    	"title": "update post",
    	"body": "update post file"
	})
})
.then((response) => response.json())
.then((json) => console.log(json));





// EDITING A POST


fetch("https://jsonplaceholder.typicode.com/posts/1", {
	method: "PUT",
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		"userId": 1,
    	"title": "update post",
    	"body": "update post file"
	})
})
.then((response) => response.json())
.then((json) => console.log(json));


// DELETING A POST



fetch("https://jsonplaceholder.typicode.com/posts/1", {
	method: "DELETE",
})