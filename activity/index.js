
// Create a fetch request using the GET method that will retrieve all the  to do list items from JSON Placeholder API.

fetch("https://jsonplaceholder.typicode.com/todos", {
	method: "GET",
	headers: {
		'Content-Type': 'application/json'
	}
})
.then((response) => response.json())
.then((json) => console.log(json));


// Using the data retrieved, create an array using the map method to return just the title of every item and print the result in the console.




/*let numbers = [1, 2, 3, 4, 5];
console.log("Before the map method: "+numbers);

let numberMap = numbers.map(function(number){
	return number*number;
});

console.log("Result of map method: ");
console.log(numberMap);
console.log("");
*/


//5 Create a fetch request using the GET method that will retrieve a single to do list item from JSON Placeholder API.


fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "GET",
	headers: {
		'Content-Type': 'application/json'
	}
})
.then((response) => response.json())
.then((json) => console.log(json));



// 6 Using the data retrieved, print a message in the console that will provide the title and status of the to do list item.

// 7 Create a fetch request using the POST method that will create a to do list item using the JSON Placeholder API.


fetch("https://jsonplaceholder.typicode.com/todos/", {
	method: "POST",
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		"userId": 1,
    	"completed": "false",
    	"title": "add to do",
    	"toDoList": "to eat"
	})
})
.then((response) => response.json())
.then((json) => console.log(json));


// 8 Create a fetch request using the PUT method that will update a to do list item using the JSON Placeholder API.

fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PUT",
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		"userId": 1,
    	"completed": "true",
    	"title": "update to do",
    	"toDoList": "to sleep"
	})
})
.then((response) => response.json())
.then((json) => console.log(json));



// 9 Update a to do list item by changing the data structure to contain the following properties: title, description, status, date completed, user id

fetch("https://jsonplaceholder.typicode.com/todos/99", {
	method: "PUT",
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
    	"Title": "update to do",
    	"Description": "to sleep",
    	"Status": "Completed",
    	"Date Completed": "Jan 01, 2023",
    	"User ID": "99"
	})
})
.then((response) => response.json())
.then((json) => console.log(json));


// 10. Create a fetch request using the PATCH method that will update a to do list item using the JSON Placeholder API.


// 11 Update a to do list item by changing the status to complete and add a date when the status was changed.

fetch("https://jsonplaceholder.typicode.com/todos/3", {
	method: "PUT",
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
    	"completed": "true",
		"userId": 3,
    	"title": "fugiat veniam minus",
    	"Date Completed": "Jan 01, 2023"

	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// 12 Create a fetch request using the DELETE method that will delete an item using the JSON Placeholder API.



fetch("https://jsonplaceholder.typicode.com/todos/44", {
	method: "DELETE",
})
